package server;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import rmi.EchoInt;

public class EchoObjectRMI {

	public static void main(String[] args) throws AlreadyBoundException 
        {
			try 
            {
                Registry registry = LocateRegistry.createRegistry(7000);
                EchoObject objeto = new EchoObject();
				EchoInt stub = (EchoInt) UnicastRemoteObject.exportObject(objeto, 0);
				registry.rebind("echo", stub);
			} 
            catch (RemoteException e) 	
            {
				e.getMessage();
				System.err.println("Ocurrio algo malo en el extremo remoto" + " " +  e.toString());
	            System.exit(-1); // can't just return, rmi threads may not exit
			}
			System.out.println("el servidor de echo esta listo");
	}
}