package server;

import java.net.*;
import java.text.*;
import java.util.*;
import rmi.EchoInt;


public class EchoObject implements EchoInt {
   String myURL="localhost";

public EchoObject()
   {
      try 
      {
         myURL=InetAddress.getLocalHost().getHostName();
      } 
      catch (UnknownHostException e) 
      {
         myURL="localhost";
      }
   } 

public String echo(String input)
   {
      Date h = new Date();
      String fecha = DateFormat.getTimeInstance(3,Locale.FRENCH).format(h);
      String ret = "hola" +  fecha + "> " +  input;
      try 
      {
         Thread.sleep(3000);  ret = ret + " (retrasada 3 segundos)";
      } 
      catch (InterruptedException e) 
      {
         e.getMessage();
      }

      return ret;
   }
}
